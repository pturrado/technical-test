package reader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import com.pturrado.technicalTest.framework.reader.Reader;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for Reader.class
 */
public class ReaderTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public ReaderTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(ReaderTest.class);
  }

  public void testFileNotExists() {
    final File file = new File("reader.xml");
    try (final Reader reader = new Reader(file, null)) {
      fail("IllegalArgumentException expected");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().length() > 0);
    } catch (IOException e) {
      fail(e.getMessage());
    }
  }

  public void testReader() {
    int actual = 0;
    final File file = new File("pom.xml");
    final BlockingQueue<String> queue = new ArrayBlockingQueue<>(Short.MAX_VALUE);
    try (final Reader reader = new Reader(file, queue)) {
      reader.start();
    } catch (IOException e) {
      fail(e.getMessage());
    } finally {
      actual = queue.size();
    }

    int expected = 0;
    try (final LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file))) {
      while (lineNumberReader.skip(Long.MAX_VALUE) > 0);
      expected = lineNumberReader.getLineNumber();
    } catch (IOException e) {
      fail(e.getMessage());
    }

    assertEquals(expected, actual);
  }
}
