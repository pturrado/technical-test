package com.pturrado.technicalTest.framework.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import org.slf4j.LoggerFactory;
import com.pturrado.technicalTest.framework.tools.Worker;
import ch.qos.logback.classic.Logger;

public class Reader extends Worker<String> {

  final Logger logger = (Logger) LoggerFactory.getLogger(Reader.class);

  public Reader(final File target, final BlockingQueue<String> queue) throws FileNotFoundException {
    super(target, queue);
    if (!target.exists()) {
      throw new IllegalArgumentException(this.getClass().getSimpleName()
          + "\tTarget file doesn't exists. [" + target.getAbsolutePath() + "]");
    }
  }

  @Override
  public void run() {
    try (final FileReader fileReader = new FileReader(this.target)) {
      try (final BufferedReader bufferedReader = new BufferedReader(fileReader)) {
        boolean canContinue = true;
        do {
          final String line = bufferedReader.readLine();
          if (line == null) {
            canContinue = !this.isClosing();
          } else {
            this.queue.put(line);
            this.logger.trace("[Read]\t{}", line);
          }
        } while (canContinue);
      }
    } catch (IOException e) {
      this.logger.warn(e.toString());
    } catch (InterruptedException e) {
      this.logger.error(e.toString());
      Thread.currentThread().interrupt();
    }
  }
}
