package writer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import com.pturrado.technicalTest.framework.writer.Writer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class WriterTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public WriterTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(WriterTest.class);
  }

  public void testFileAlreadyExists() {
    final File file = new File("pom.xml");
    try (final Writer writer = new Writer(file, null)) {
      fail("IllegalArgumentException expected");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().length() > 0);
    } catch (IOException e) {
      fail(e.getMessage());
    }
  }

  public void testWriter() {
    final File file = new File(this.getClass().getSimpleName() + ".txt");
    final BlockingQueue<String> queue = new ArrayBlockingQueue<>(Short.MAX_VALUE);
    queue.add(this.getName());
    int actual = queue.size();
    try (final Writer writer = new Writer(file, queue)) {
      writer.start();
    } catch (IOException e) {
      fail(e.getMessage());
    } finally {
      assertTrue(queue.isEmpty());
      assertTrue(file.exists());
    }

    int expected = 0;
    try (final LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file))) {
      while (lineNumberReader.skip(Long.MAX_VALUE) > 0);
      expected = lineNumberReader.getLineNumber();
    } catch (IOException e) {
      fail(e.getMessage());
    } finally {
      assertTrue(file.delete());
    }

    assertEquals(expected, actual);
  }
}
