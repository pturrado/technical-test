package com.pturrado.technicalTest.framework.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import org.slf4j.LoggerFactory;
import com.pturrado.technicalTest.framework.tools.Worker;
import ch.qos.logback.classic.Logger;

public final class Writer extends Worker<String> {

  final Logger logger = (Logger) LoggerFactory.getLogger(Writer.class);

  public Writer(final File target, final BlockingQueue<String> queue) {
    super(target, queue);
    if (target.exists()) {
      throw new IllegalArgumentException(this.getClass().getSimpleName()
          + " Target file already exists. [" + target.getAbsolutePath() + "]");
    }
  }

  @Override
  public void run() {
    try (final FileWriter fileWriter = new FileWriter(this.target)) {
      try (final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
        final String lineSeparator = System.getProperty("line.separator");
        boolean canContinue = true;
        do {
          final String line = this.queue.poll(50L, TimeUnit.MILLISECONDS);
          if (line == null) {
            canContinue = !this.isClosing();
          } else {
            bufferedWriter.append(line + lineSeparator);
            this.logger.trace("[Write]\t{}", line);
          }
        } while (canContinue);
      }
    } catch (IOException e) {
      this.logger.warn(e.getMessage());
    } catch (InterruptedException e) {
      this.logger.error(e.toString());
      Thread.currentThread().interrupt();
    }
  }
}
