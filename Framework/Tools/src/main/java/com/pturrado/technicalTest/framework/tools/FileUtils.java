package com.pturrado.technicalTest.framework.tools;

import java.io.File;

public class FileUtils {

  public static final String LINE_SEPARATOR = System.getProperty("line.separator");
  public static final String EXTENSION_SEPARATOR = ".";

  public static String getFileExtension(final String file) {
    final Integer index = file.lastIndexOf(EXTENSION_SEPARATOR);
    return (Integer.compare(index, -1) != 0) ? file.substring(index + 1) : file;
  }

  public static String getFilename(final String file) {
    final Integer begin = Math.max(0, file.lastIndexOf(File.pathSeparator));
    final Integer index = file.lastIndexOf(EXTENSION_SEPARATOR);
    return (Integer.compare(index, -1) != 0) ? file.substring(begin, index) : file;
  }
}
