package com.pturrado.technicalTest.framework.tools;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Worker<T extends Object> extends Thread implements Closeable {

  private final AtomicBoolean close = new AtomicBoolean(false);
  protected final File target;
  protected final BlockingQueue<T> queue;

  public Worker(final File target, final BlockingQueue<T> queue) {
    if (target == null) {
      throw new IllegalArgumentException(
          this.getClass().getSimpleName() + " Target file is 'null'");
    }
    this.target = target;
    this.queue = queue;
  }

  protected boolean isClosing() {
    return this.close.get();
  }

  @Override
  public void close() throws IOException {
    this.close.set(true);
    try {
      this.join();
    } catch (InterruptedException e) {
      System.out.println(e.getMessage());
      Thread.currentThread().interrupt();
    }
  }
}
