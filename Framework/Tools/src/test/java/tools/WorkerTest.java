package tools;

import java.io.File;
import java.io.IOException;
import com.pturrado.technicalTest.framework.tools.Worker;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple Worker.
 */
public class WorkerTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public WorkerTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(WorkerTest.class);
  }

  public void testFileNull() {
    try (final Worker<Object> worker = new Worker<>(null, null)) {
      fail("IllegalArgumentException expected");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().length() > 0);
    } catch (IOException e) {
      fail(e.getMessage());
    }
  }

  public void testWorker() {
    final File target = new File("pom.xml");
    try (final Worker<Object> worker = new Worker<>(target, null)) {
      worker.close();
      assertFalse(worker.isAlive());
    } catch (IOException e) {
      fail(e.getMessage());
    }
  }
}
