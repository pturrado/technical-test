package tools;

import com.pturrado.technicalTest.framework.tools.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple FileUtils.
 */
public class FileUtilsTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public FileUtilsTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(FileUtilsTest.class);
  }

  public void testGetFileExtension() {
    assertEquals("xml", FileUtils.getFileExtension("pom.xml"));
    assertEquals("folder", FileUtils.getFileExtension("folder"));
    assertEquals("project", FileUtils.getFileExtension(".project"));
    assertEquals("yml", FileUtils.getFileExtension(".gitlab-ci.yml"));
  }

  public void testGetFilename() {
    assertEquals("pom", FileUtils.getFilename("pom.xml"));
    assertEquals("folder", FileUtils.getFilename("folder"));
    assertEquals("", FileUtils.getFilename(".project"));
  }
}
