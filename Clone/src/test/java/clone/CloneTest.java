package clone;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import com.pturrado.technicalTest.clone.Clone;

public class CloneTest {

  private final String resourceFolder = System.getProperty("user.dir") + File.separator + "src"
      + File.separator + "test" + File.separator + "resources" + File.separator;
  private final String resourceFile = resourceFolder + "bible.txt";

  private final String targetDirectory = System.getProperty("user.dir") + File.separator + "target"
      + File.separator + "test" + File.separator;
  
  @Before
  public void cleanUp() throws IOException {
    final File directory = new File(this.targetDirectory);
    FileUtils.forceMkdir(directory);
    FileUtils.cleanDirectory(directory);
  }

  @Test(expected = IOException.class)
  public void testCloneNullInput() throws IOException {
    new Clone(null);
  }

  @Test
  public void testCloneToFile() throws IOException {
    final String input = this.targetDirectory + "testCloneToFile.txt";
    FileUtils.copyFile(new File(resourceFile), new File(input));

    final Clone clone = new Clone(input);
    clone.toFile();

    final String result = this.targetDirectory + "testCloneToFile-clone.txt";
    final File resultFile = new File(result);
    assertTrue(resultFile.exists());
    assertTrue(FileUtils.contentEquals(new File(resourceFile), resultFile));

    assertTrue(FileUtils.deleteQuietly(new File(input)));
    assertTrue(FileUtils.deleteQuietly(resultFile));
  }

  @Test
  public void testCloneToFileNullOutput() throws IOException {
    final String input = this.targetDirectory + "testCloneToFileNullOutput.txt";
    FileUtils.copyFile(new File(resourceFile), new File(input));

    final Clone clone = new Clone(input);
    clone.toFile(null);

    final String result = this.targetDirectory + "testCloneToFileNullOutput-clone.txt";
    final File resultFile = new File(result);
    assertTrue(resultFile.exists());
    assertTrue(FileUtils.contentEquals(new File(resourceFile), resultFile));

    assertTrue(FileUtils.deleteQuietly(new File(input)));
    assertTrue(FileUtils.deleteQuietly(resultFile));
  }

  @Test
  public void testCloneToFileCustomOutput() throws IOException {
    final String input = this.targetDirectory + "testCloneToFileCustomOutput.txt";
    FileUtils.copyFile(new File(resourceFile), new File(input));

    final String expected = this.targetDirectory + "testCloneToFileCustomOutput-expected.txt";
    final Clone clone = new Clone(input);
    clone.toFile(expected);

    final File resultFile = new File(expected);
    assertTrue(resultFile.exists());
    assertTrue(FileUtils.contentEquals(new File(resourceFile), resultFile));

    assertTrue(FileUtils.deleteQuietly(new File(input)));
    assertTrue(FileUtils.deleteQuietly(resultFile));
  }

  @Test
  public void testCloneMainNoArgs() {
    final String[] args = new String[] {};
    Clone.main(args);
  }

  @Test
  public void testCloneMainInput() throws IOException {
    final String input = this.targetDirectory + "testCloneMainInput.txt";
    FileUtils.copyFile(new File(resourceFile), new File(input));

    final String[] args = new String[] {input};
    Clone.main(args);

    final String result = this.targetDirectory + "testCloneMainInput-clone.txt";
    final File resultFile = new File(result);
    assertTrue(resultFile.exists());
    assertTrue(FileUtils.contentEquals(new File(resourceFile), resultFile));

    assertTrue(FileUtils.deleteQuietly(new File(input)));
    assertTrue(FileUtils.deleteQuietly(resultFile));
  }

  @Test
  public void testCloneMainInputAndOutput() throws IOException {
    final String input = this.targetDirectory + "testCloneMainInputAndOutput.txt";
    FileUtils.copyFile(new File(resourceFile), new File(input));

    final String expected = this.targetDirectory + "testCloneMainInputAndOutput-expected.txt";
    final String[] args = new String[] {input, expected};
    Clone.main(args);

    final File resultFile = new File(expected);
    assertTrue(resultFile.exists());
    assertTrue(FileUtils.contentEquals(new File(resourceFile), resultFile));

    assertTrue(FileUtils.deleteQuietly(new File(input)));
    assertTrue(FileUtils.deleteQuietly(resultFile));
  }
}
