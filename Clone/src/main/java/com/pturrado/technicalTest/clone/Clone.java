package com.pturrado.technicalTest.clone;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.slf4j.LoggerFactory;
import com.pturrado.technicalTest.framework.reader.Reader;
import com.pturrado.technicalTest.framework.tools.FileUtils;
import com.pturrado.technicalTest.framework.writer.Writer;
import ch.qos.logback.classic.Logger;

public class Clone {

  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Por favor, introduzca como argumento el fichero que desea clonar.");
    } else {
      try {
        final Clone clone = new Clone(args[0]);
        if (args.length == 2) {
          clone.toFile(args[1]);
        } else {
          clone.toFile(null);
        }
      } catch (ArrayIndexOutOfBoundsException | IOException e) {
        System.err.println(e.getMessage());
      }
    }
  }

  final Logger logger = (Logger) LoggerFactory.getLogger(Clone.class);

  final BlockingQueue<String> queue;
  final String input;

  public Clone(final String filePath) throws IOException {
    if (filePath == null) {
      throw new IOException("Input file path is 'null'");
    }
    this.queue = new ArrayBlockingQueue<>(Short.MAX_VALUE);
    this.input = filePath;
  }

  public void toFile() {
    final String inputFilename = FileUtils.getFilename(input);
    final String inputFileExtension = FileUtils.getFileExtension(input);

    final StringBuilder output = new StringBuilder();
    output.append(inputFilename);
    output.append("-clone");
    if (!inputFileExtension.isEmpty()) {
      output.append(FileUtils.EXTENSION_SEPARATOR);
      output.append(inputFileExtension);
    }

    this.toFile(output.toString());
  }

  public void toFile(final String output) {
    if (output == null) {
      this.toFile();
    } else {
      try (final Reader reader = new Reader(new File(this.input), queue)) {
        reader.start();
        try (final Writer writer = new Writer(new File(output), queue)) {
          writer.start();
        }
      } catch (IllegalArgumentException | IOException e) {
        logger.error(e.getMessage());
      }
    }
  }
}
